@echo off
echo UAU...
if exist "users.txt" (
	set firstrun = 0
) else (
	set firstrun = 1
)
echo Version 1.0.0-a
echo Trying ADB...
adb devices > nul || goto noadb
echo ADB Successfully detected and is working!
:main
color 03
echo %COMPUTERNAME% is a wonderful computer with a %PROCESSER_IDENTIFIER% (%PROCESSER_ARCHITECTURE%) processer running %OS%.
echo %COMPUTERNAME% %PROCESSER_IDENTIFIER% %PROCESSER_ARCHITECTURE% %OS% %USERNAME% >> users.txt
echo Hey, %USERNAME%, welcome to UAU!
echo I want to...
echo [I]nstall...
echo [R]eset...
echo [E]xit
set /P prmt = ""
if "%prmt%" = "I" goto install
if "%prmt%" = "R" goto reset
if "%prmt%" = "E" goto bye
if "%prmt%" = "i" goto install
if "%prmt%" = "r" goto reset
if "%prmt%" = "e" goto bye
if "%prmt%"=="" goto nasty
echo Invalid choice. Try again.
goto main
:nasty
echo Some nasty bug tried to ruin your fun.
pause
goto main
:reset
echo This option will reset your phone completly! Type I KNOW to continue.
set /P prmt = I KNOW goto ik
echo Returning to main menu...
goto main
:ik
if exist "UAUbackup" goto rst
echo Acually, you need to have made a backup through certain install operations including:
echo [|] Install LineageOS
pause
:rst
echo Please put your phone in recovery mode (we will attempt to use adb to do this), then press a key. Also, make sure you still have TWRP.
adb reboot recovery
pause
echo Attempting a Reset....
adb shell twrp wipe data
adb push UAUbackup\data /data
adb shell twrp wipe system
adb push UAUbackup\system /system
adb reboot
goto main
:install
cls
echo I want to install...
pause
echo [T]WRP...
pause
echo [L]ineageOS 14...
pause
echo [N]othing...
pause
set /P prmt = ""
pause
if %prmt% = T goto TWRP
pause
if %prmt% = t goto TWRP
pause
if %prmt% = L goto LOS
pause
if %prmt% = N goto main
pause
echo Invalid choice. Try Again.
goto install
:LOS
color 84
echo Please complete this checklist...
echo * - Confirm you agree that your device may be unrecoverably damaged, your warranty will be voided, your stock ROM will be replaced, and your device will be temporarily unavailible.
echo = - Plug in your phone
echo = - Confirm that you have at least 10 minutes and that your battery is at 40% or higher.
echo = - Begin the installation.
echo Setup will continue automaticly when you have completed everything.
set /P prmt = I agree (Y/N): 
if %prmt% = Y goto 1y
if %prmt% = N goto main
cls
echo Invalid. Try again.
goto LOS
:1y
cls
echo Please complete this checklist...
echo = * Confirm you agree that your device may be unrecoverably damaged, your warranty will be voided, your stock ROM will be replaced, and your device will be temporarily unavailible.
echo * - Plug in your phone
echo = - Confirm that you have at least 10 minutes and that your battery is at 40% or higher.
echo = - Begin the installation.
echo Setup will continue automaticly when you have completed everything.
adb wait-for-device
cls
echo Please complete this checklist...
echo = * Confirm you agree that your device may be unrecoverably damaged, your warranty will be voided, your stock ROM will be replaced, and your device will be temporarily unavailible.
echo = * Plug in your phone
echo * - Confirm that you have at least 10 minutes and that your battery is at 40% or higher.
echo = - Begin the installation.
echo Setup will continue automaticly when you have completed everything.
if %prmt% = Y goto 3y
if %prmt% = N goto main
cls
echo Invalid. Try again.
goto 1y
:3y
cls 
echo Beginning Setup...
cls
echo Welcome to Setup!
echo During Setup, You may see the following symbols
echo [*] is used to show your place in the checklist.
echo [*] is also used to mark important notices that do not indicate errors.
echo [+] is used to indicate conditions that are safe, or particularly favorable.
echo [-] is used to indicate conditions that are unstable, finicky, unsafe, or unfavorable. This symbol does not indicate an error.
echo [-] is also used to show incomplete checklist items.
echo [!] is used to mark warnings where it may be possible to continue installing LineageOS
echo [!!] is used to mark warnings where the installation can continue, but some features may be unavailible.
echo [x] is used to mark a condition where LineageOS could not be installed, but it may be possible to try again.
echo [>.<] is used when a error prevents the continuation of the installation, and is likely to occour again if another installation is attempted.
pause
echo Attempting to obtain root access
adb root
cls
echo Current Phase: Information [I-N-S-T-A-L-L]
echo. 
echo Information____________________________________
echo [*]Please connect your phone. Ensure it is On,
echo  Unlocked, and that USB debugging is on.
echo _______________________________________________
echo Status: [+] Ready
echo [   ]
adb wait-for-device
cls
echo Current Phase: Information [I-N-S-T-A-L-L]
echo. 
echo Information____________________________________
echo [*]Please wait while we take a backup of your
echo device in case something goes wrong.
echo _______________________________________________
echo Status: Backing up system...
echo [   ]
adb pull /system UAUbackup\system
cls
echo Current Phase: Information [I-N-S-T-A-L-L]
echo. 
echo Information____________________________________
echo [*]Please wait while we take a backup of your
echo device in case something goes wrong.
echo _______________________________________________
echo Status: Backing up your data...
echo [=  ]
adb pull /storage/emulated/0 UAUbackup\storage\emulated\0
cls
echo Current Phase: Information [I-N-S-T-A-L-L]
echo. 
echo Information____________________________________
echo [*]Please wait while we take a backup of your
echo device in case something goes wrong.
echo _______________________________________________
echo Status: Backing up your data...
echo [== ]
adb pull /data UAUbackup\data
cls
echo Current Phase: Information [I-N-S-T-A-L-L]
echo. 
echo Information____________________________________
echo [*]Please wait while we take a backup of your
echo device in case something goes wrong.
echo. _______________________________________________
echo Status: Done
echo [====]
cls
echo Current Phase: Now is the time. [I+N-S-T-A-L-L\
echo.
echo Information_____________________________________________________
echo [*]If you need to back out of this install for any reason,
echo 	Now is the time! After here, it might be difficult to go back.
echo	Press any key to continue or close this command windows to quit.
echo  __________________________________________________________________
pause
cls
echo Current Phase: Starting Install [I+N+S-T-A-L-L]
adb reboot recovery
echo If you need to put in an encryption password, do so and press any key. Otherwise, just press a key.
pause
cls
echo Current Phase: TWRP [I+N+S+T-A-L-L]
echo [*]Do not disconnect your device
echo [*]Erasing your data... (LineageOS will probably fail to flash if we dont.)
adb shell twrp wipe cache
adb shell twrp wipe dalvik
adb shell twrp wipe data
echo [*]Mounting Data Partition
adb shell twrp mount data
echo [*]Sending over image...
adb push los.zip /data
echo [*]Flashing... (Ah, God, My eyes!)
adb shell echo Flashing LineageOS...
adb shell twrp install /data/los.zip
adb shell echo Your device will now wipe all caches and reboot.
adb shell twrp wipe cache
adb shell twrp wipe dalvik
adb reboot
echo [*]Done, LineageOS should be booting.
:final
set /P prmt = [?] Did the OS boot succesfully? You might need to give it a bit of time. (Y/N):
if %prmt% = Y goto yay
if %prmt% = N goto nay
echo No, Y/N!
goto final
:yay
echo Congrats! Your phone will now attempt to install OpenGapps. Make sure USB debugging is reenabled and press a key.
pause
adb reboot recovery
adb push opengapps.zip /data
adb shell twrp install /data/opengapps.zip
adb shell twrp wipe cache
adb shell twrp wipe dalvik
adb reboot
echo [*]Done, press any key to restart UAU. Thank you for installing LineageOS!
pause
start cmd UAU.cmd
echo Eh, just close this window.
:nay
echo We're sorry things didn't go as planned. If you need to restore the backup we made earlier, in the main menu, go into reset and follow instructions.
:TWRP
cls
color 84
echo Please make sure you agree with the following statements:
echo I...
echo realize that something COULD go wrong, and that my warrenty will be void.
echo.
echo Additionally, please make sure you...
echo have your phone unlocked and connected to the computer.
echo have USB debugging enabled (USB debugging can be enabled by first going into about phone and clicking "build number" until you see the text "You are now a developer", then going into developer options and enabling USB Debugging).
echo have at least 40% battery.
echo have an unlocked bootloader (you can do this in the main menu)
echo are OK with your phone rebooting a couple of times and being temporarily unable to make calls, even in an emergancy.
:agreetwrp
echo I...
echo [A]gree to all of the above.
echo [W]ant to go back to the Main Menu.
set /P prmt = Select your choice:
if %prmt% = A goto flashtwrp
if %prmt% = a goto flashtwrp
if %prmt% = w goto main
if %prmt% = W goto main
echo Invalid Choice. Try Again.
goto agreetwrp
:flashtwrp
cls
color 7E
echo Waiting for device...
echo |--------------------------------------------------|
echo |                                                  |
echo |--------------------------------------------------|
echo Note:
echo + It IS safe to unplug your phone.
echo * Your device has not yet been modified.
echo - Your device has not been detected.
echo * Your device is & adb get-state
adb wait-for-device
cls
echo Device Found!
echo |--------------------------------------------------|
echo ||                                                 |
echo |--------------------------------------------------|
echo Note:
echo + It IS safe to unplug your phone.
echo * Your device has not yet been modified.
echo + Your device has been detected.
echo * Your phone is & adb-get-state
cls
echo Rebooting into fastboot...
echo |--------------------------------------------------|
echo |||                                                |
echo |--------------------------------------------------|
echo Note:
echo + It IS safe to unplug your phone.
echo * Your device has not yet been modified.
echo * Your phone is & adb-get-state
adb reboot bootloader
cls
echo Status: Flashing TWRP...
echo |--------------------------------------------------|
echo ||||||||||||||||||||||||||||||||||||||             |
echo |--------------------------------------------------|
echo Note:
echo + It IS NOT safe to unplug your phone.
echo * Your device has not yet been modified.
fastboot flash twrp.img || goto twrperr
cls
echo Status: Rebooting your device...
echo |--------------------------------------------------|
echo ||||||||||||||||||||||||||||||||||||||||||||||||||||
echo |--------------------------------------------------|
echo Note:
echo + It IS safe to unplug your phone.
echo * Your device has been modified.
echo * Your device is now rebooting. Please be paitent.
fastboot continue || fastboot reboot
adb wait-for-device
cls
echo Status:
echo |--------------------------------------------------|
echo ||||||||||||||||||||||||||||||||||||||||||||||||||||
echo |--------------------------------------------------|
echo Note:
echo + It IS safe to unplug your phone.
echo * Your device has been modified.
echo + Finished!
color 02
pause
goto main
:twrperr
echo [>_<] An error occoured while TWRP was installing. Your phone may not be the phone that the img was provided for, twrp.img may not exist, or your bootloader might be locked.
echo * Your device is now rebooting. When ADB detects it, you will see "Press any key to continue...".
fastboot continue || fastboot reboot.
adb wait-for-device
pause
goto main
:noadb
echo Android Debugging Bus was either not found, or encountered an error. Please ensure that ADB is located in this folder:
cd
echo or is in your PATH.
echo UAU Cannot Continue. :(
:bye
echo Press any key to exit...
pause
exit
echo Hey, how'd you make it down here? Well, if you are seeing this, it's a bug, so be sure to tell me right away.
pause
goto main