@echo off
:beginning
cd | set UAUPATH=
set PATH=%PATH%";"%UAUPATH%"\ADB;"
color 0d
echo Universal Android Utility...
IF NOT EXIST config.ini echo VERSION=2.1-alpha > config.ini
echo Reading from Configuration...
find "debug=" c:\windows\win.ini | sort /r | date | find "=" > en#er.bat
echo set value=%%5> enter.bat
call en#er.bat
del en?er.bat > nul
echo For debug,
echo Value is %value%.
:main
cls
echo Main Menu
echo I want to...
echo [F]ix
echo [R]eset
echo [I]nstall something onto
echo [C]ustomize
echo [K]ill
echo ...my device
CHOICE /C FRICK /M "Fill in the blank: "
IF ERRORLEVEL = 1 goto FIX
IF ERRORLEVEL = 2 GOTO RESET
IF ERRORLEVEL = 3 GOTO INSTALL
IF ERRORLEVEL = 4 GOTO CUSTOMIZE
IF ERRORLEVEL = 5 GOTO KILL
:INSTALL
cls
echo What would you like to install?
echo [L]ineageOS
echo [T]WRP
echo [S]uperSU
echo [N]othing
CHOICE /C LTSN
IF ERRORLEVEL = 1 goto los
if ERRORLEVEL = 2 goto twrp
if ERRORLEVEL = 3 goto supsu
goto main
:los
cls
echo Make sure that:
echo a. TWRP is installed.
echo b. Your device is turned on, unlocked, and USB debugging is enabled, or TWRP is booted.
echo c. You have a decent amount of battery power left
echo d. The LineageOS image for your device is in the flashables directory and is called los.zip
echo e. Your device is connected to the computer
echo f. You have BACKED UP YOUR DATA. THIS OPERATION WILL WIPE ALL YOUR DATA!
pause
adb reboot recovery
echo If you need to enter encryption password, do so and
pause
adb shell twrp wipe data
adb shell twrp wipe cache
adb shell twrp wipe dalvik
adb push flashables\los.zip /sdcard
adb shell twrp install /sdcard/los.zip
pause
adb reboot
goto install
:RESET
echo Are you sure you wish to wipe all data on device? (Make sure twrp is in flashables and called twrp.img)
choice /C YN
IF ERRORLEVEL = 2 goto MAIN
echo Please put the device in fastboot and
pause
fastboot boot flashables\twrp.img
echo Now erasing...
adb shell twrp erase data
adb shell twrp erase cache
adb shell twrp erase dalvik
adb reboot
pause
goto main
:FIX
echo Starting troubleshooter...
cls
echo Troubleshoot
echo What seems to be the problem?
echo My device refuses to [c]harge.
echo My device does not play any [s]ound.
echo My device randomly [h]eats up and shuts down and/or disables charging.
echo My device [b]ootloops (boot animation does not go away or device restarts while booting more than 5 times in a row).
echo I want to go back to the [m]ain menu.
choice /C cshbdm /M "What Would you like to do?"
IF ERRORLEVEL = 1 goto charge
IF ERRORLEVEL = 2 GOTO sound
IF ERRORLEVEL = 3 GOTO heat
IF ERRORLEVEL = 4 GOTO bootlopp
IF ERRORLEVEL = 0 GOTO data
IF ERRORLEVEL = 5 goto main
:charge
echo This could be do to a variety of factors. Generally, however, this is caused by a faulty connection.
echo If you know what you are doing, you can open up the device to the circut level and try and find one.
echo Otherwise, it's probably best that you buy a new device.
pause
goto fix
:sound
echo Sound issues are generally a software issue. If your phone refuses to play sound out of the box,
echo take or send it back to the place of purchase. If it started doing this recently, try backing up your phone and doing a factory reset.
echo If you recently flashed a custom ROM based on Android Nougat, this is a common issue. Try flashing another ROM.
echo If nothing above works, and you know what you are doing, you may want to attempt repairs at the circut board level
echo Otherwise, you may want to consider a new device.
pause
goto fix
:heat
start https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwiAqI-4rLLYAhVCyoMKHbSeCEcQFggsMAE&url=https%3A%2F%2Fwww.asurion.com%2Fconnect%2Ftech-tips%2Fhow-hot-is-too-hot-5-tips-to-keep-your-phone-from-overheating%2F&usg=AOvVaw1AtyQbgCsQ6GNogIaAWguv
goto fix
:bootloops
cls
echo Oh, Finally, Something FUN!
echo If your device is bootlooping:
echo Reflash any recently flashed software
echo The lovely method of a factory reset is not out of the question.
echo would you like to let me try to fix it?
CHOICE /C YN /M "Press Y for yes or N for no: "
if ERRORLEVEL = 2 goto fix
:tryagain
echo Please put your phone in fastboot mode and
pause
echo Checking for devices...
fastboot devices || goto tryagain
echo The following device was found:
fastboot devices
echo Please put the appropriate TWRP img in the "Flashables" directory and called "twrp.img".
pause
fastboot boot flashables\twrp.img
adb shell twrp install /sdcard/substratum/substratumrescuelegacy.zip
echo Attempted to flash substratum rescue, if you do not use substratum or have no clue what it is, ignore any errors above.
echo Would you like me to try to flash LineageOS?
CHOICE /C YN
if ERRORLEVEL = 2 goto rebootfix
echo Trying to flash LineageOS...
echo Please make sure the LineageOS image for your device is in the "Flashables" directory and called "los.zip" and
pause
echo Flashing...
adb push /flashables\los.zip /sdcard
adb shell twrp wipe data
adb shell twrp wipe cache
adb shell twrp wipe dalvik
adb shell twrp install /sdcard/los.zip
adb reboot
echo I guess we will see if this fixed it...
pause
goto fix
:fixreboot
adb reboot>nul
fastboot reboot>nul
goto fix